import Vue from 'vue'
import Vuelidate from 'vuelidate'
import Paginate from 'vuejs-paginate'
import VueMeta from 'vue-meta'
import App from './App.vue'
import router from './router'
import store from './store'
import dateFilter from '@/filters/date.filter'
import currencyFilter from '@/filters/currency.filter'
import localizeFilter from '@/filters/localize.filter'
import tooltipDirective from '@/directives/tooltip.directive'
import titlePlugin from '@/utils/title.plugin'
import messagePlugin from '@/utils/message.plugin'
import Loader from '@/components/app/Loader'
import './registerServiceWorker'
import 'materialize-css/dist/js/materialize.min'

import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'

Vue.config.productionTip = false;

Vue.use(messagePlugin);
Vue.use(titlePlugin)
Vue.use(Vuelidate);
Vue.use(VueMeta)

Vue.filter('date', dateFilter);
Vue.filter('currency', currencyFilter);
Vue.filter('localize', localizeFilter);

Vue.directive('tooltip', tooltipDirective)

Vue.component('Loader', Loader);
Vue.component('Paginate', Paginate)

firebase.initializeApp({
  apiKey: "AIzaSyCfjkLWpX8NvCcvZQA-EWgjremDbwt_VpU",
  authDomain: "vue-crm-5f731.firebaseapp.com",
  databaseURL: "https://vue-crm-5f731.firebaseio.com",
  projectId: "vue-crm-5f731",
  storageBucket: "vue-crm-5f731.appspot.com",
  messagingSenderId: "763959121242",
  appId: "1:763959121242:web:5382f5255aaa35c93c4f63",
  measurementId: "G-8VYCHGGWBW"
})

let app

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount('#app');
  }
})

